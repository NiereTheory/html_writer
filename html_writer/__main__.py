import argparse

from html_writer import HTMLWriter  # from package as __init__.py handled package syntax


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m", "--message", dest="message", help="Enter a message", required=True
    )
    args = vars(parser.parse_args())

    print(HTMLWriter.write_simple_tag("li", [args["message"]]))


if __name__ == "__main__":
    main()
