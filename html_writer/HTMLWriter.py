class HTMLWriter:
    def __init__(self):
        pass

    @staticmethod
    def write_simple_tag(tag: str, vals: list) -> str:
        tagged = [f"<{tag}>{val}</{tag}>" for val in vals]
        return "".join(tagged)
