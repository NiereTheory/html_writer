import pytest
from html_writer.HTMLWriter import HTMLWriter


class TestHTMLWriter:
    def setup_class(self):
        self.hw = HTMLWriter()

    def test_write_simple_tag(self) -> None:
        assert self.hw.write_simple_tag("li", ["test"]) == "<li>test</li>"
        assert (
            self.hw.write_simple_tag("td", ["test", "test2"])
            == "<td>test</td><td>test2</td>"
        )
