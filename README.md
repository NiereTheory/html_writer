# html_writer

This is a simple project to implement html and python string compatibility

Installation: `pip install git+https://gitlab.com/NiereTheory/html_writer.git`

CLI use:
```bash
html_writer -m "my_message"
```

Python use:
```python
from html_writer import HTMLWriter
HTMLWriter.write_simple_tag("li", ["my_message"]) 
```

For testing:
```python
pytest -v
```